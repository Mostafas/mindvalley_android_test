package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mostafa on 11/3/2016.
 * this is the main class to parse JSON data with Gson
 */

public class MainJSON {

    @SerializedName("id")
    private String itemId;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("likes")
    private int likes;

    @SerializedName("liked_by_user")
    private boolean userLiked;

    @SerializedName("user")
    private UserJSON user;

    @SerializedName("links")
    private LinksJSON links;

    @SerializedName("urls")
    private UrlsJSON urls;

    @SerializedName("categories")
    private List<CategoriesJSON> categories;


    public String getItemId() {
        return itemId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getLikes() {
        return likes;
    }

    public boolean getUserLiked() {
        return userLiked;
    }

    public UserJSON getUser() {
        return user;
    }

    public LinksJSON getLinks() {
        return links;
    }

    public UrlsJSON getUrls() {
        return urls;
    }

    public List<CategoriesJSON> getCategories() {
        return categories;
    }

}
