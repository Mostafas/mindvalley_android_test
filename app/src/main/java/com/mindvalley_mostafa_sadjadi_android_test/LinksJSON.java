package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mostafa on 11/3/2016.
 * this is the class to get Gson parsed links
 */

public class LinksJSON {

    @SerializedName("self")
    private String self;

    @SerializedName("html")
    private String html;

    @SerializedName("download")
    private String download;

    public String getSelf() {
        return self;
    }

    public String getHtml() {
        return html;
    }

    public String getDownload() {
        return download;
    }

}
