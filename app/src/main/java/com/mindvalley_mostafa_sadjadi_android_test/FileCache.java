package com.mindvalley_mostafa_sadjadi_android_test;

/**
 * Created by Mostafa on 11/3/2016.
 * its for caching files inside Disk ( i know its was not necessary but i put it anyway)
 */

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;

public class FileCache {

    private File cacheDir;

    public FileCache(final Context context) {
        new Thread(new Runnable() {
            public void run() {
                //Find the dir to save cached images
                if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
                    cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "TTImages_cache");
                else
                    cacheDir = context.getCacheDir();
                if (!cacheDir.exists())
                    cacheDir.mkdirs();
            }
        }).start();


    }


    public File getFile(String url) {

        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename = String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }

}