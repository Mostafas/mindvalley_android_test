package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mostafa on 11/3/2016.
 * this is the class to get Gson parsed userLinks in JSON
 */

public class UserLinks {

    @SerializedName("self")
    private String self;

    @SerializedName("html")
    private String html;

    @SerializedName("photos")
    private String photos;

    @SerializedName("likes")
    private String likes;

    public String getSelf() {
        return self;
    }

    public String getHtml() {
        return html;
    }

    public String getPhotos() {
        return photos;
    }

    public String getLikes() {
        return likes;
    }

}
