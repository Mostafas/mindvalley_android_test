package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mostafa on 11/3/2016.
 * this is the class to get Gson parsed user profile info
 */

public class UserProfileImageJSON {

    @SerializedName("small")
    private String small;

    @SerializedName("medium")
    private String medium;

    @SerializedName("large")
    private String large;

    public String getSmall() {
        return small;
    }

    public String getMedium() {
        return medium;
    }

    public String getLarge() {
        return large;
    }

}
