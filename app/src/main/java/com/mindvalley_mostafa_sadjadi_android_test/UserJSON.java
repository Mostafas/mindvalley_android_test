package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mostafa on 11/3/2016.
 * this is the class to get Gson parsed user info
 */

public class UserJSON {


    @SerializedName("id")
    private String userId;

    @SerializedName("username")
    private String username;

    @SerializedName("name")
    private String name;

    @SerializedName("profile_image")
    private UserProfileImageJSON profileImage;

    @SerializedName("links")
    private UserLinks userLinks;

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public UserProfileImageJSON getProfileImage() {
        return profileImage;
    }

    public UserLinks userLinks() {
        return userLinks;
    }


}
