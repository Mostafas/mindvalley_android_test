package com.mindvalley_mostafa_sadjadi_android_test;

/**
 * Created by Mostafa on 11/4/2016.
 */

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;
import java.util.Date;


public class GeneralOperationsClass {


    public String getPublishedDate(String publishedDate) {
        publishedDate = publishedDate.replace("T", " ");
        //publishedDate = publishedDate.substring(0,18);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;
        try {
            date = sdf.parse(publishedDate);
        } catch (Exception e) {
            return "";
        }
        return new PrettyTime().format(date);

    }
}
