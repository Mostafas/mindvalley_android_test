package com.mindvalley_mostafa_sadjadi_android_test;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    //private PagingListView lv_timeLine;
    private ListView lv_timeLine;
    private Adapter adapter;
    private List<MainJSON> feed;
    private GeneralOperationsClass operations;
    private boolean pullToRefresh = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDropBox()
                .build());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //lv_timeLine = (PagingListView) findViewById(R.id.lv_timeLine);//initializing the ListView
        lv_timeLine = (ListView) findViewById(R.id.lv_timeLine);//initializing the ListView
        //set the paging to false-because we dont have more data to load
        //lv_timeLine.onFinishLoading(false, null);
        lv_timeLine.setNestedScrollingEnabled(true);
        feed = new ArrayList<>();//initializing the List of items
        adapter = new Adapter(this, feed);//initializing the adapter
        operations = new GeneralOperationsClass();//instantiate a General Operation class

        //pull to refresh initialization
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        //loading data to list
        new PostFetcher().execute();

    }

    //pull to refresh- onRefresh
    @Override
    public void onRefresh() {
        //when user pull the list ro refresh it calls the async method to show updated data
        pullToRefresh = true;
        new PostFetcher().execute();
    }

    //async class to load data into the listView
    public class PostFetcher extends AsyncTask<Void, Void, String> {
        //private static final String TAG = "PostFetcher";
        //public static final String SERVER_URL = "http://builder1.cloudapp.net/articles/0/20";
        private String SERVER_URL = "http://pastebin.com/raw/wgkJgazE";
        private final String USER_AGENT = "Mozilla/5.0";

        @Override
        protected String doInBackground(Void... params) {
            try {

                URL url = new URL(SERVER_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                //connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestMethod("POST");

                //this is in case that we had to add some parameters in the body
                /*String urlParameters = "publishers=" + operations.getSelectedPublishers(Home.this);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters);
                dStream.flush();
                dStream.close();*/

                //reading the JSON
                final BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                //System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                }
                br.close();

                //parse the JSON we got and get it as desired a dataType using Gson
                Gson gson = new GsonBuilder().create();
                Type type = new TypeToken<List<MainJSON>>() {
                }.getType();

                //if user pulled to refresh we load new data
                //if (pullToRefresh) {
                List<MainJSON> tempItems;// = new ArrayList<>();
                tempItems = gson.fromJson(responseOutput.toString(), type);
                feed.clear();
                for (MainJSON item : tempItems) {
                    feed.add(item);
                }
                pullToRefresh = false;
                //} else {//otherwise add data to the end of the list-- which we dont have

                //}
                //addMoreItems(tempItems, loadMore);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            adapter.notifyDataSetChanged();
                            lv_timeLine.setAdapter(adapter);
                            lv_timeLine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    /*if (view.getId() == R.id.iv_timeLine_main_pic){
                                        Toast.makeText(MainActivity.this,  , Toast.LENGTH_SHORT).show();
                                    }*/
                                }

                            });

                            swipeRefreshLayout.setRefreshing(false);//hide the refresh icon

                        } catch (Exception ee) {

                        }

                    }
                });

            } catch (MalformedURLException mURLx) {
                Log.e("MalformedURL", mURLx.getLocalizedMessage());
            } catch (IOException IOe) {
                Log.e("IOException ", IOe.getLocalizedMessage());
            } /*catch (JSONException Je) {
                Log.e("JSONException", Je.getLocalizedMessage());
            }*/

            return null;
        }
    }

}
