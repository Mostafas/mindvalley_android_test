package com.mindvalley_mostafa_sadjadi_android_test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.like.LikeButton;

import java.util.ArrayList;
import java.util.List;


public class Adapter extends BaseAdapter {

    private List<MainJSON> itemsList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private MyViewHolder mViewHolder;
    private Loader imageLoader;
    private MainJSON currentItem;


    public Adapter(Context context, List<MainJSON> myList) {
        this.itemsList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public MainJSON getItem(int position) {
        return itemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final String like = context.getString(R.string.likes);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.time_line_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        currentItem = getItem(position);
        GeneralOperationsClass operation = new GeneralOperationsClass();
        imageLoader = new Loader(context);//zero means default cache limit


        mViewHolder.tv_userName.setText(currentItem.getUser().getName());
        mViewHolder.tv_cat.setText(getCategories());
        mViewHolder.like_btn.setLiked(currentItem.getUserLiked());
        mViewHolder.tv_likes.setText(String.valueOf(currentItem.getLikes())+" "+like);
        mViewHolder.tv_published_time.setText(operation.getPublishedDate(currentItem.getCreatedAt()));

        //final PagingListView lv_feed = ((PagingListView) parent);
       /* mViewHolder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                lv_feed.performItemClick(view, position, 0);

            }
        });*/

        try {
            //ImageManager imgManager = new ImageManager(context);
            //imgManager.displayImage(currentItem.getUrls().getSmall(),(Activity) context,mViewHolder.iv_mainPic);
            imageLoader.DisplayImage(currentItem.getUrls().getSmall(), mViewHolder.iv_mainPic);
            imageLoader.DisplayImage(currentItem.getUser().getProfileImage().getSmall(), mViewHolder.iv_pUserPic);

        } catch (Exception ex) {

        }

        return convertView;
    }

    //view holder for items which needs to be fill with data
    private class MyViewHolder {
        TextView tv_userName, tv_cat, tv_likes, tv_published_time;
        LikeButton like_btn;
        //ImageView iv_pic,iv_plus,iv_minus;
        ImageView iv_mainPic, iv_pUserPic;
        //Button btn_cancel;

        public MyViewHolder(View item) {
            tv_userName = (TextView) item.findViewById(R.id.tv_timeLine_publisher_username);
            like_btn = (LikeButton) item.findViewById(R.id.lb_timeLine_like_button);
            tv_published_time = (TextView) item.findViewById(R.id.tv_timeLine_publish_time);
            tv_likes = (TextView) item.findViewById(R.id.tv_timeLine_likes_count);
            tv_cat = (TextView) item.findViewById(R.id.tv_timeLine_image_category);
            iv_mainPic = (ImageView) item.findViewById(R.id.iv_timeLine_main_pic);
            iv_pUserPic = (ImageView) item.findViewById(R.id.iv_timeLine_publisher_user_pic);
            //btn_cancel = (Button) item.findViewById(R.id.btn_timeLine_cancel);
        }
    }

    private String getCategories() {

        String temp = "";
        for (CategoriesJSON cat : currentItem.getCategories()) {
            if (!temp.isEmpty()) {
                temp += "," + cat.getCatTitle();
            } else {
                temp = cat.getCatTitle();
            }
        }

        return temp;

    }
}
