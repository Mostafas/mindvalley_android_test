package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mostafa on 11/3/2016.
 * this is the class to get Gson parsed category info(in the JSON file its an array
 * so in the MainJSON class its defined as a List
 */

public class CategoriesJSON {


    @SerializedName("id")
    private String catId;

    @SerializedName("title")
    private String catTitle;

    @SerializedName("photo_count")
    private int photoCount;

    @SerializedName("links")
    private CatLinks catLinks;

    public String getCatId() {
        return catId;
    }

    public String getCatTitle() {
        return catTitle;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public CatLinks getCatLinks() {
        return catLinks;
    }

}
