package com.mindvalley_mostafa_sadjadi_android_test;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mostafa on 11/4/2016.
 * this is the class to get Gson parsed Categories Links
 */

public class CatLinks {

    @SerializedName("self")
    private String catLinksSelf;

    @SerializedName("photos")
    private String catLinksPhotos;


    public String getCatLinksSelf() {
        return catLinksSelf;
    }

    public String getCatLinksPhotos() {
        return catLinksPhotos;
    }



}
